from django.contrib import admin

# Register your models here.
from .models import Car, User, News, Transaction, Question, Choice

admin.site.register(Car)
admin.site.register(User)
admin.site.register(News)
admin.site.register(Transaction)
admin.site.register(Question)
admin.site.register(Choice)

