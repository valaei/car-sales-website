import datetime

from django.db import models
from django.utils import timezone

# Create your models here.

class Car(models.Model):
    BMW = 'BMW'
    BENZ = 'Benz'
    VOLKSWAGEN = 'Wolks Wagen'
    JEEP = 'Jeep'
    MAZDA = 'Mazda'
    BENTLEY = 'Bentley'
    TOYOTA = 'Toyota'
    LEXUS = 'Lexus'
    RANGEROVER = 'Range Rover'
    GASOLINE = 'Gasoline'
    DIESEL = 'Diesel'
    ELECTRIC = 'Electric'
    HYBRID = 'Hybrid'
    SEDAN = 'Sedan'
    SUV = 'SUV'
    HATCHBACK = 'Hatchback'
    COUPE = 'Coupe'
    CONVERTIBLE = 'Convertible'
    MINIVAN = 'Minivan'
    VAN = 'Van'
    TRUCK = 'Truck'
    OFFROAD = 'Off-Road'
    ROADSTER = 'RoadSter'
    LIMOUSINE = 'Limousine'
    V4 = 'V4'
    V6 = 'V6'
    V8 = 'V8'
    V12 = 'V12'
    BLACK = 'Black'
    GRAY = 'Gray'
    ASHY = 'Ashy'
    WHITE = 'White'
    LIGHT_BLUE = 'Light Blue'
    DARK_BLUE = 'Dark Blue'
    METALLIC_BLUE = 'Metallic Blue'
    RED = 'Red'
    YELLOW = 'Yellow'
    GREEN = 'Green'
    CAMOUFLAGE = 'Camouflage'
    PURPLE = 'Purple'
    SPECIAL = 'Special'
    S4 = 'S4'
    S6 = 'S6'
    S8 = 'S8'
    S12 = 'S12'
    GERMANY = 'Germany'
    UK = 'UK'
    USA = 'USA'
    JAPAN = 'Japan'

    BRAND_CHOICES = (
        (BMW, 'BMW'),
        (BENZ, 'Benz'),
        (VOLKSWAGEN, 'Wolks Wagen'),
        (JEEP, 'Jeep'),
        (MAZDA, 'Mazda'),
        (BENTLEY, 'Bentley'),
        (TOYOTA, 'Toyota'),
        (LEXUS, 'Lexus'),
        (RANGEROVER, 'Range Rover'),
    )
    MOTOR_TYPE_CHOICES = (
        (GASOLINE, 'Gasoline'),
        (DIESEL, 'Diesel'),
        (ELECTRIC, 'Electric'),
        (HYBRID, 'Hybrid'),
    )
    CAR_TYPE_CHOICES = (
        (SEDAN, 'Sedan'),
        (SUV, 'SUV'),
        (HATCHBACK, 'Hatchback'),
        (COUPE, 'Coupe'),
        (CONVERTIBLE, 'Convertible'),
        (MINIVAN, 'Minivan'),
        (VAN, 'Van'),
        (TRUCK, 'Truck'),
        (OFFROAD, 'Off-Road'),
        (ROADSTER, 'RoadSter'),
        (LIMOUSINE, 'Limousine'),
    )
    CYLINDER_COUNT_CHOICES = (
        (V4, 'V4'),
        (V6, 'V6'),
        (V8, 'V8'),
        (V12, 'V12'),
    )
    COLOR_CHOICES = (
        (BLACK, 'Black'),
        (GRAY, 'Gray'),
        (ASHY, 'Ashy'),
        (WHITE, 'White'),
        (LIGHT_BLUE, 'Light Blue'),
        (DARK_BLUE, 'Dark Blue'),
        (METALLIC_BLUE, 'Metallic Blue'),
        (RED, 'Red'),
        (YELLOW, 'Yellow'),
        (GREEN, 'Green'),
        (CAMOUFLAGE, 'Camouflage'),
        (PURPLE, 'Purple'),
        (SPECIAL, 'Special'),
    )
    SEATS_COUNT_CHOICES = (
        (S4, '4'),
        (S6, '6'),
        (S8, '8'),
        (S12, '12'),
    )
    ORDERED_BY_CHOICES = (
        (GERMANY, 'Germany'),
        (UK, 'UK'),
        (USA, 'USA'),
        (JAPAN, 'Japan'),
    )

    def __year_choices():
        return [(r,r) for r in range(2000, datetime.date.today().year+1)]
    def __str__(self):
        return self.name

    name = models.CharField(max_length=100)
    brand = models.CharField(choices=BRAND_CHOICES, max_length=50)
    car_type = models.CharField(choices=CAR_TYPE_CHOICES, max_length=50)
    motor_type = models.CharField(choices=MOTOR_TYPE_CHOICES, max_length=50)
    product_year = models.IntegerField(choices=__year_choices())
    mileage =  models.IntegerField(default=0)
    cylinder_count = models.CharField(choices=CYLINDER_COUNT_CHOICES, max_length=50)
    color = models.CharField(choices=COLOR_CHOICES, max_length=50)
    seats_count = models.CharField(choices=SEATS_COUNT_CHOICES, max_length=10)
    ordered_by = models.CharField(choices=ORDERED_BY_CHOICES, max_length=20)
    options = models.TextField()
    price =  models.IntegerField(default=0)
    main_image = models.ImageField(upload_to='media/car') 
    second_image = models.ImageField(upload_to='media/car', blank=True) 
    third_image = models.ImageField(upload_to='media/car', blank=True) 
    forth_image = models.ImageField(upload_to='media/car', blank=True) 
    fifth_image = models.ImageField(upload_to='media/car', blank=True) 
    pub_date = models.DateTimeField(default=timezone.now)


class User(models.Model):
    def __str__(self):
        return self.name
    
    name = models.CharField(max_length=200)
    national_id = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=50) 
    passport_image = models.ImageField(upload_to='media/user/', blank=True) 
    address = models.TextField()
    email = models.EmailField()
    introducer = models.CharField(max_length=100)
    username = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    point =  models.IntegerField(default=0)
    pub_date = models.DateTimeField(default=timezone.now)

class News(models.Model):
    def __str__(self):
        return self.title

    title = models.CharField(max_length=200)
    summary = models.CharField(max_length=300)
    background_image = models.ImageField(upload_to='media/news') 
    main_image = models.ImageField(upload_to='media/news', blank=True) 
    article = models.TextField(blank=True)
    pub_date = models.DateTimeField(default=timezone.now)

class Transaction(models.Model):
    BUY_FROM_ORIGIN_COUNTRY = 'BuyFromOriginCountry'
    TRANSMISSION_TO_IRAN = 'TransmissionToIran'
    CHECK_IN_BORDER = 'CheckInBorder'
    CUSTOMS_HOUSE_RELEASE = 'CustomsHouseRelease'

    CAR_STATUS_CHOICES = (
        (BUY_FROM_ORIGIN_COUNTRY, 'Buy From Origin Country'),
        (TRANSMISSION_TO_IRAN, 'Transmission To Iran'),
        (CHECK_IN_BORDER, 'Check In Border'),
        (CUSTOMS_HOUSE_RELEASE, 'Customs House Release'),
    )

    def __str__(self):
        return self.title

    title = models.CharField(max_length=200)
    car = models.OneToOneField(Car, on_delete=models.CASCADE, primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    payed_amount =  models.IntegerField(default=0)
    total_price = models.IntegerField(default=0)
    entrance_date = models.DateField(blank=True)
    exit_date = models.DateField(blank=True)
    extended_date = models.DateField(blank=True)
    entrance_border = models.CharField(max_length=200)
    driver_name = models.CharField(max_length=200)
    driver_number = models.CharField(max_length=100)
    driver_passport = models.ImageField(upload_to='media/driver/', blank=True) 
    car_status = models.CharField(choices=CAR_STATUS_CHOICES, max_length=50)
    about = models.TextField(blank=True)
    pub_date = models.DateTimeField(default=timezone.now)


class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    
    def __str__(self):
        return self.question_text

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
    
    def __str__(self):
        return self.choice_text

