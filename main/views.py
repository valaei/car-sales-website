from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic
from flask import request

from .models import News, Car, Question, Choice

# Create your views here.
class IndexView(generic.ListView):
    template_name = 'main/index.html'
    context_object_name = 'res_data'

    def get_queryset(self):
            latest_news = News.objects.order_by('-pub_date')[:3]
            if not latest_news:
                return {
                    'error': "News not Found! PLease insert news.",
                }
            latest_cars = Car.objects.order_by('-pub_date')[:12]
            if not latest_cars:
                return {
                    'error': "car not Found! PLease insert car.",
                }
            return {
                'latest_news': latest_news,
                'latest_cars': latest_cars,
            }

class DetailView(generic.DetailView):
    model = Question
    template_name = 'main/detail.html'


class ResultsView(generic.DetailView):
    model = Question
    template_name = 'main/results.html'

def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'main/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('main:results', args=(question.id,)))

